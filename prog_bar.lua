local phone_debug = false
local d=require("dt")
local req_prefix
if phone_debug == true then
  req_prefix = "/storage/emulated/0/qlua5/scripts/bat_indicator/"
else
  req_prefix = ""
end
local pusher = require(req_prefix.."pusher")

local progress_bar = {
  default_bar = {}
}

progress_bar.default_bar.border = {
  {1,{2,0,5,3,2,0}},
  {1,{2,0,1,3,3,nil,1,3,2,0}},
  {1,{3,3,3,nil,3,3}},
  {12,{1,3,7,nil,1,3}},
  {1,{9,3}},
  width = 9,
  height = 16,
  gaps = {[2]=6,[3]=4,[4]=4}
}

progress_bar.default_bar.step = 1/14

function progress_bar.default_bar.chargecolor(charge)
  local result = 3
  local full = 1 - progress_bar.default_bar.step
  if charge < full then
    if charge < progress_bar.default_bar.step / 2 then
      result = 0
    elseif charge < 0.29 then
      result = 1
    else
      result = 2
    end
  end
  return result
end

function progress_bar.fill(charge, pbar_pattern)
  local result = {}
  local pbar = pbar_pattern or progress_bar.default_bar
  local charge_color = pbar.chargecolor(charge)
  local lines = math.floor(1 / pbar.step) - math.floor(charge / pbar.step)
  local filler
  for i , block in ipairs(pbar.border) do
    local part = block[2]
    for j = 1, block[1] do
      if lines > 0 then
        filler = 0
        lines = lines - 1
      else
        filler = charge_color
      end
      local filled_line = pusher.cp_table(part)
      if pbar.border.gaps[i] then
        filled_line[pbar.border.gaps[i]] = filler
      end
      pusher.push(result, filled_line)
    end
  end
  result = pusher.pack_pairs(result)
  result.height, result.width = pbar.border.height, pbar.border.width
  return result
end

return progress_bar