local debugTable = {}
local dt = debugTable
dt.mt = {}
dt.assertions = {
   maxDepth = function() return 2 end
   ,
   proxy = function(component_name)
      if component_name == 'mfe' then
         local functable = { 
            ['getCapacity'] = function() return 4000000 end,
            ['getEUStored'] = function() return 2000000 end
         }
         return functable
      end
   end
}

function dt.printTable(theTable, orderTable, ident)
   if type(theTable) ~= 'table' and not ident then
      print(tostring(theTable))
      return nil
   end
   local function output(k, v)
      local strMarker_k = ''
      if type(k) == 'string' then
         strMarker_k = '\''
      end
      local strMarker_v = ''
      if type(v) == 'string' then
         strMarker_v = '\''
      end
      if type(v) == 'table' then
         print(ident .. '[' .. strMarker_k .. tostring(k) .. strMarker_k .. '] = ')
         dt.printTable(v, orderTable, ident .. '\t')
      elseif k~= nil and v~=nil then
         print(ident .. '[' .. strMarker_k .. tostring(k) .. strMarker_k .. '] = ' .. strMarker_v .. tostring(v) .. strMarker_v)
      elseif v~= nil then
         print(ident .. '[' .. strMarker_k .. tostring(k) .. strMarker_k .. '] = ' .. strMarker_v .. tostring(v) .. strMarker_v)
      end
   end


   ident = ident or ""
   print(ident .. '=== table start ==')

   if orderTable == nil then
      for k, v in pairs(theTable) do
         output(k,v)
      end
   else
      local dict = {}
      for i = 1, #orderTable do
         output(orderTable[i], theTable[orderTable[i]])
         dict[orderTable[i]] = true
      end
      for k, v in pairs(theTable) do
         if not dict[k] then
            output(k,v)
         end
      end
   end
   print(ident .. '=== table end ====')
end

function dt.printLuaTable(tbl,recursive)
   if type(theTable) ~= 'table' and not ident then
      print(tostring(tbl))
      return nil
   end
   local function print_local(value, brackets)
      brackets = brackets or false
      if type(value) == "number" then
         io.write(tostring(value))
      else
         if brackets then
            io.write('["')
            io.write(tostring(value))
            io.write('"]=')
         else
            io.write('"')
            io.write(tostring(value))
            io.write('"')
         end
      end
   end
   io.write("{")
   local comma = ""
   for k, v in pairs(tbl) do
      io.write(comma)
      if type(k) ~= 'number' then
         print_local(k,true)
      end
      if type(v) == "table" then
         dt.printLuaTable(v,true)
      else
         print_local(v)
      end
      comma = ","
   end
   if recursive then
      io.write("}")
   else
      io.write("}\n")
   end
end


function dt.mt.__index(table, value)
   if value ~= nil then
      local name = tostring(value)
      print('got  ' .. name)
      if dt.assertions[name] then
         return dt.assertions[name]
      end
      local stubTable = {}
      stubTable[1] = name
      setmetatable(stubTable, getmetatable(table))
      return stubTable
   end
end

function dt.mt.__silentIndex(table, value)
   if value ~= nil then
      local name = tostring(value)
      local stubTable = {}
      stubTable[1] = name
      setmetatable(stubTable, getmetatable(table))
      return stubTable
   end
end


function dt.mt.__call(table, ...)
   local argCount = select('#',...)
   print('and called with')
   if argCount == 0 then
      print('nil')
   else
      for i=1, argCount do
         print('[' .. tostring(i) .. ']' .. ' = ' .. tostring(select(i,...)))
      end
   end
   result = dt.assertions[table[1]]
   if result ~= nil then
      if type(result) == 'function' then
         return result()
      else
         local function closure()
            return result
         end
         return closure
      end
   else
      return ''
   end
end

function dt.mt.__silentCall(table, ...)
   result = dt.assertions[table[1]]
   if result ~= nil then
      if type(result) == 'function' then
         return result()
      else
         return result
      end
   else
      return ''
   end
end


function dt.set(preset, params)
   local lmt = dt.mt
   local localObj
   if preset == nil then
      localObj = {}
   else
      localObj = preset
   end
   if params ~= nil then
      if params.showCalls == false then
         lmt.__index = lmt.__silentIndex
         lmt.__call = lmt.__silentCall
      end
   end
   if type(localObj) == 'table' then
      for k, v in pairs(localObj) do
         if type(v) == 'table' then
            dt.set(v)
         end
      end
   end
   setmetatable(localObj, lmt)
   return localObj, type(localObj)
end

dt.sides = {
  bottom = 0, down = 0 , negy = 0,
  top = 1, up = 1, posy = 1,
  back = 2, north = 1, negz = 1,
  front = 3, south = 3, posz = 3, forward = 3,
  right = 4, west = 4, negx = 4,
  left = 5, east = 5, posx = 5
}

return dt