local b32 = require("bit32")
local phone_debug = false
local req_prefix
if phone_debug == true then
  req_prefix = "/storage/emulated/0/qlua5/scripts/bat_indicator/"
else
  req_prefix = ""
end
local glypher = {
glyphs = require(req_prefix.."5_8"),
picture_1={384,576,576,576,576,576,
3696,12872,21064,16392,16398,16389,16385,
16385,16386,8196,4104,2064,2064,["width"]=15,["height"]=19}}

function glypher.pack_glyph(tbl,glyph_height)
  local result = {}
  local glyph_width = math.floor(#tbl / glyph_height)
  result.height = glyph_height
  result.width = glyph_width
  for line = 1, glyph_height do
    local packed = 0
    for bit = 1, glyph_width do
      local index = math.floor((line-1)*glyph_width + bit)
      local value = tbl[index] or 0
      local bit_mask = b32.lshift(value, bit-1)
      packed = b32.bor(packed, bit_mask)
    end
    result[line] = packed
  end
  return result
end

function glypher.unpack_glyph(packed,values)
  values = values or {[true]=1, [false]=0}
  local result = {}
  local display_true = values[true] or true
  local display_false = values[false] or false
  result.height = packed.height
  result.width = packed.width
  for line = 1, packed.height do
    local mask = 1
    for bit = 1, packed.width do
      index = (line - 1) * packed.width + bit
      if b32.btest(packed[line], mask) then
        result[index] = display_true
      else
        result[index] = display_false
      end
      mask = b32.lshift(mask, 1)
    end
  end
  return result
end

function glypher.patternize(glyph)
  local resultflat = {}
  local pusher = require("pusher")
  for i = 0, glyph.height - 1 do
    local curline = {}
    for j = 1, glyph.width do
      pusher.push(curline, glyph[i*glyph.width + j])
    end
    pusher.push(resultflat, curline)
  end
  local result = pusher.pack_pairs(resultflat)
  result.width = glyph.width
  result.height = glyph.height
  return result
end

function glypher.symbol(id, color)
  color = color or 1
  local char = glypher.glyphs[id]
  if char then
    local unpacked = glypher.unpack_glyph(char, {[true]=color,[false]=0})
    return glypher.patternize(unpacked)
  end
end

return glypher