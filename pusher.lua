local push = {}

function push.cp_table(tbl)
   local result = {}
   for k, v in pairs(tbl) do
    if type(v) ~= "table" then
       result[k] = v
    else
       result[k] = push.cp_table(v)
    end
   end
   return result
end

function push.equal_tables(t1, t2)
  for i = 1, #t1 do
    if t1[i] ~= t2[i] then
      return false
    end
  end
  return true
end

function push.equal(v1,v2)
  if type(v1) ~= type(v2) then
    return false
  else
    if type(v1) == 'table' and type(v2) == 'table' then
      if #v1 == #v2 then
        return push.equal_tables(v1, v2)
      else
        return false
      end
    else
      return v1==v2
    end
  end
end

function push.push(tbl, value, push_copy)
  if push.equal(tbl[#tbl], value) then
    tbl[#tbl-1] = tbl[#tbl-1] + 1
  else
    table.insert(tbl,1)
    if push_copy then
      table.insert(tbl,pusher.cp_table(value))
    else
      table.insert(tbl,value)
    end
  end
end

function push.pack_pairs(tbl)
  local result = {}
  for i = 1, #tbl, 2 do
    table.insert(result,{tbl[i], tbl[i+1]})
  end
  return result
end

return push