
local prefix = ""
local com, event, core, glypher, s, dt, prog_bar, pusher

com = require("component")
core = require("core")
glypher = require("glypher")
s = require("sides")
prog_bar = require("prog_bar")
pusher = require("pusher")
event = require("event")



local projector = com.proxy("9d3b56b7-7c40-46a9-bf51-88eb40ef41e2")
core.get_glyph = glypher.symbol
core.color_depth = projector.maxDepth()
local default_padding = {before = {line=1, char=1}, after = {line=1, char=1}}
local defaultColors = {0xaf0000, 0xafff30, 0xffffff}

local function round(value, precision)
  precision = precision or 100
  local truncated = math.floor(value * 10 * precision)
  local d, rounded = truncated % 10
  if d > 3 then d = 1 else d = 0 end
  rounded = math.floor(truncated/10) + d
  return rounded
end

local function set_colors(projector)
  local color = 1
  local inc = projector.maxDepth() - 1
  for i = 1, #defaultColors do
    projector.setPaletteColor(i, defaultColors[color])
    color = color + inc
  end
  projector.clear()
end

local slices_map = {
  width = 47,
  height = 32,
  [s.front] = {
    start = {x=2, y=32, z=1},
    pos = {x=2, y=32, z=1},
    charvector = {x=1,y=0,z=0},
    linevector = {x=0,y=-1,z=0}
  },
  [s.back] = {
    start = {x=47, y=32, z=48},
    pos = {x=47, y=32, z=48},
    charvector = {x=-1,y=0,z=0},
    linevector = {x=0,y=-1,z=0}
  },
  [s.right] = {
    start = {x=48, y=32, z=2},
    pos = {x=48, y=32, z=2},
    charvector = {x=0,y=0,z=1},
    linevector = {x=0,y=-1,z=0}
  },
  [s.left] = {
    start = {x=1, y=32, z=47},
    pos = {x=1, y=32, z=47},
    charvector = {x=0,y=0,z=-1},
    linevector = {x=0,y=-1,z=0}
  }
}

local processing ={}

local function try_to_find(name)
  local result = com.proxy(name)
  if not result and com.isAvailable(name) then
    result = com.getPrimary(name)
  end
  return result
end

local function get_charge(ftable)
  local cap = ftable.capfunc()
  local stored = ftable.stored()
  return stored/cap
end

local function display_charges()
  for slice_key, devparams in pairs(processing) do
    local slice = slices_map[slice_key]
    local dev_addr = devparams.address
    local functable = devparams.ftable
    local charge = get_charge(functable)
    local dev = try_to_find(dev_addr)
    local holo = devparams.holo
    core.clear_slice(slice, slices_map.width, slices_map.height, holo)
    if dev then
      local filled_battery_pattern = prog_bar.fill(charge)
      core.draw_pattern(holo, slice, filled_battery_pattern, default_padding)
      local text_slice = core.next_slice(slice, 1, 1 + default_padding.before.char + default_padding.after.char + filled_battery_pattern.width)
      local charge_value = round(charge)
      core.print({charge_value, "%"}, 3, holo, text_slice, default_padding, true)
    end
  end
end

local function add_device_for_processing(dev_name_or_address, side_key, projector)
  local slice = slices_map[side_key]
  if slice then
    local dev = try_to_find(dev_name_or_address)
    if dev then
      local functable = {}
      functable.capfunc = dev.getMaxEnergyStored or dev.getCapacity or dev.getEnergyCapacity
      functable.stored = dev.getEnergyStored or dev.getEUStored
      processing[side_key] = {address = dev_name_or_address, ftable = functable, holo = projector}
    end
  end
end

set_colors(projector)
add_device_for_processing("br_reactor", s.front, projector)
display_charges()
event.timer(5, display_charges, math.huge)

projector = com.proxy("c901e759-02ff-40fb-8612-f21314f30166")
set_colors(projector)
add_device_for_processing("br_reactor", s.left, projector)
display_charges()
event.timer(5, display_charges, math.huge)