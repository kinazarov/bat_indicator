local core = {}
core.unicode = require("unicode")
core.get_glyph = nil
core.color_depth = nil
core.cp_table = require("pusher").cp_table

function core.valid_coords(point)
  for k, v in pairs(point) do
    if v < 1 then return false end
    if v > 48 then
      return false
    else
      if k == 'y' and v > 32 then return false end
    end
  end
  return true
end

function core.slice_set_pos(slice, line, char)
  line = (line or 1) - 1
  char = (char or 1) - 1
  for k, v in pairs(slice.start) do
    slice.pos[k] = v + slice.charvector[k] * char + slice.linevector[k] * line
  end
end

function core.next_slice(slice, linestep, charstep)
  local result = core.cp_table(slice)
  core.slice_set_pos(result, linestep, charstep)
  result.start = core.cp_table(result.pos)
  return result
end

function core.clear_slice(slice, width, height, projector)
  if core.valid_coords({x=1,y=height, z=width}) then
    local start = core.cp_table(slice.start)
    local x, z = slice.start.x, slice.start.z
    local maxy, miny = slice.start.y, slice.start.y + slice.linevector.y * (height - 1)
    if maxy < miny then
      local t = maxy
      maxy = miny
      miny = t
    end
    for i = 1, width do
      projector.fill(x, z, miny, maxy, 0)
      x = x + slice.charvector.x
      z = z + slice.charvector.z
    end
    return true
  else
    return false
  end
end

function core.draw_pattern_line(projector, slice, line_tbl, opaque_zero)
  if #line_tbl == 0 then return false end
  local coords = {}
  for k,v in pairs(slice.pos) do
    coords[k] = v
  end
  for i = 1, #line_tbl, 2 do
    local color = line_tbl[i + 1]
    local repeats = line_tbl[i]
    for j = 1, repeats do
      if color == 0 and opaque_zero then
        color = false
      end
      if core.valid_coords(coords) and color then
        if core.color_depth == 2 then
          projector.set(coords.x, coords.y, coords.z, color)
        else
          projector.set(coords.x, coords.y, coords.z, monochrome)
        end
      end
      for k, v in pairs(coords) do coords[k] = coords[k] + slice.charvector[k] end
    end
  end
  for k, v in pairs(coords) do slice.pos[k] = coords[k] end
  return true
end

function core.draw_pattern(projector, slice, pattern, paddings, opaque_zero)
  paddings = paddings or {before = {char=0, line=0}, after = {char=0, line=0}}
  local lineIndex = 1 + paddings.before.line
  for i = 1, paddings.before.line do
    core.slice_set_pos(slice,i,1)
    core.draw_pattern_line(projector, slice, {paddings.before.char + pattern.width + paddings.after.char, 0}, opaque_zero)
    lineIndex = lineIndex + 1
  end
  for num_part, part in ipairs(pattern) do
    local repeat_part = part[1]
    local line = part[2]
    for i = 1, repeat_part do
      core.slice_set_pos(slice, lineIndex)
      core.draw_pattern_line(projector, slice, {paddings.before.char, 0}, opaque_zero)
      core.draw_pattern_line(projector, slice, line, opaque_zero)
      core.draw_pattern_line(projector, slice, {paddings.after.char, 0}, opaque_zero)
      lineIndex = lineIndex + 1
    end
  end
  for i = 1, paddings.after.line do
    core.slice_set_pos(slice, lineIndex, 1)
    core.draw_pattern_line(projector, slice, {paddings.before.char + pattern.width + paddings.after.char, 0}, opaque_zero)
    lineIndex = lineIndex + 1
  end
end

function core.get_glyphs(input, color)
  local result = {' '}
  for i, value in ipairs(input) do
    if core.get_glyph(value) then
      table.insert(result, #result, core.get_glyph(value, color))
    else
      if type(value) == 'number' then
        local digit = value
        local insertion_pos = #result
        repeat
          local remainer = math.fmod(digit, 10)
          digit = math.floor(digit / 10)
          local pattern = core.get_glyph(remainer, color)
          table.insert(result, insertion_pos, pattern)
        until digit == 0
      elseif type(value) == 'string' then
        for j = 1, core.unicode.len(value) do
          local char = core.unicode.sub(value, j, j)
          local pattern = core.get_glyph(char, color)
          if pattern then
            table.insert(result, #result, pattern)
          end
        end
      end
    end
  end
  table.remove(result, #result)
  return result
end

function core.print(input, color, projector, slice, paddings, opaque_zero)
  local current_slice = core.cp_table(slice)
  local ordered_glyphs = core.get_glyphs(input, color)
  for num_pattern, pattern in ipairs(ordered_glyphs) do
    core.draw_pattern(projector, current_slice, pattern, paddings, opaque_zero)
    current_slice = core.next_slice(current_slice, 1, paddings.before.char + paddings.after.char + pattern.width)
  end
end

return core